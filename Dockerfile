FROM node:alpine
RUN mkdir -p /usr/src/weather-app
WORKDIR /usr/src/weather-app
COPY . .
RUN npm install
# EXPOSE 3000
CMD [ "node", "weather-app" ]